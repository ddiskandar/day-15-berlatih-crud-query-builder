@extends('layouts.master')

@section('title', 'Daftar Pemain Film')

@section('content')
<div>
    @if (session('status'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="mb-3">
        <a href="/cast/create" class="btn btn-primary">Tambah Data</a>
    </div>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col" >Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $cast)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <form action="/cast/{{ $cast->id }}" method="POST">
                            <a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Show</a>
                            <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="inline-block my-1 btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
