@extends('layouts.master')

@section('title')
    Detail Pemain Film #{{  $cast->id  }}
@endsection

@section('content')
<div>
    <h4>{{ $cast->nama }}</h4>
    <p>{{ $cast->umur }}</p>
    <p>{{ $cast->bio }}</p>
</div>
@endsection
