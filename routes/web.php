<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome')->name('home');

Route::get('cast', 'CastController@index');
Route::get('cast/create', 'CastController@create');
Route::post('cast', 'CastController@store');
Route::get('cast/{cast_id}', 'CastController@show');
Route::get('cast/{cast_id}/edit', 'CastController@edit');
Route::put('cast/{cast_id}', 'CastController@update');
Route::delete('cast/{cast_id}', 'CastController@destroy');
